package com.bway.springproject.dao;

import java.util.List;

import com.bway.springproject.model.Employee;

public interface EmployeeDao {

	void addEmp(Employee emp);

	void deleteEmp(int id);

	void updateEmp(Employee emp);

	Employee getbyId(int id);

	List<Employee> getAllEmp();
	
	}
