package com.bway.springproject.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bway.springproject.HomeController;
import com.bway.springproject.dao.EmployeeDao;
import com.bway.springproject.dao.UserDao;
import com.bway.springproject.model.User;
import com.bway.springproject.utils.VerifyRecaptcha;

@Controller
public class LoginController {
	
	
	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
	
	
	
	
	@Autowired
	private UserDao udao;
	@Autowired
	private EmployeeDao edao;
	
	@RequestMapping(value="/login",method=RequestMethod.GET)
	public String getLoginForm() {
		logger.info("loading log in form");
		return "login";
	}
	
	
	@RequestMapping(value="/login",method=RequestMethod.POST)
	public String userLogin(@ModelAttribute User user,Model model,HttpSession session,HttpServletRequest request ) throws IOException {
		String gcode=request.getParameter("g-recaptcha-response");
		boolean result=VerifyRecaptcha.verify(gcode);
		if(result) {
		
		user.setPassword(DigestUtils.md5DigestAsHex(user.getPassword().getBytes()));
		User u=udao.login(user.getUsername(), user.getPassword());
		
	 
		 if(u!=null) {
			 logger.info("login success");
			session.setAttribute("active", u);
			session.setMaxInactiveInterval(150);
			model.addAttribute("user",user.getUsername() );
			model.addAttribute("elist",edao.getAllEmp() );
			return "home";
		}
		 else {
		 logger.info("failed");
		model.addAttribute("error", "user does not exist");
		return "login";
	}
		}
	
		logger.info("failed");
		model.addAttribute("error", "you are not Human");
		return "login";
	
	}
	
	@RequestMapping(value="/logout")
	public String logout(HttpSession session) {
		logger.info("logout Success");
		session.invalidate();
		return "login";
	}

	@RequestMapping(value = "/facebook", method = RequestMethod.GET)
	public String fbLogin(){
		
		//String secret_key = "c85c3bbaeded9ce1ea6af891cc8733c2";
		//String app_id = "1439123129660532";
		
		return "redirect:https://www.facebook.com/dialog/oauth?client_id=194270868605738&redirect_uri=http://localhost:8081/springproject/authorize/facebook&response_type=code&scope=email";
	}
	
	
	
	@RequestMapping(value = "/authorize/facebook", method = RequestMethod.GET)
	public String saveFbUser(String code, Model model, HttpServletRequest request){
		
		model.addAttribute("elist", edao.getAllEmp());
		
	      return "home";
	      
	      
	      
	      }
	      

}
