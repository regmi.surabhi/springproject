 package com.bway.springproject.controller;

import java.io.File;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class GalleryController {
	
	@RequestMapping(value="/gallery")
	public String gallery(Model model ,HttpSession session) {
		if(StringUtils.isEmpty(session.getAttribute("active"))){
			return "login";
		}
		String[] nameList= new File("C:\\Users\\Surabhi\\Documents\\workspace-sts-3.9.11.RELEASE\\springproject\\src\\main\\webapp\\resources\\imgs").list();
		model.addAttribute("nameList",nameList);
		return "gallery";
	}

}
