package com.bway.springproject.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bway.springproject.dao.EmployeeDao;
import com.bway.springproject.model.Employee;

@Controller
public class EmployeeController {

	@Autowired
	private EmployeeDao edao;

	@RequestMapping(value = "/employee")
	public String empForm(Model model,HttpSession session) {
		
		if(StringUtils.isEmpty(session.getAttribute("active"))){
			return "login";
		}

		model.addAttribute("emodel", new Employee());

		return "employeeForm";
	}

	@RequestMapping(value = "/employee", method = RequestMethod.POST)
	public String saveEmp(@ModelAttribute Employee emp,HttpSession session) {
		
		if(StringUtils.isEmpty(session.getAttribute("active"))){
			return "login";
		}
		edao.addEmp(emp);
		return "redirect:employee";
	}

	@RequestMapping(value = "/{id}/delete")
	public String delete(@PathVariable("id") int id, Model model,HttpSession session) {

		if(StringUtils.isEmpty(session.getAttribute("active"))){
			return "login";
		}
		edao.deleteEmp(id);
		model.addAttribute("elist", edao.getAllEmp());
		return "home";
	}

	@RequestMapping(value = "/{id}/edit")
	public String edit(@PathVariable("id") int id, Model model,HttpSession session) {
		if(StringUtils.isEmpty(session.getAttribute("active"))){
			return "login";
		}
		model.addAttribute("emodel", edao.getbyId(id));
		return "editForm";
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String update(@ModelAttribute Employee emp, Model model,HttpSession session) {
		if(StringUtils.isEmpty(session.getAttribute("active"))){
			return "login";
		}
		edao.updateEmp(emp);
		model.addAttribute("elist", edao.getAllEmp());
		return "home";
	}
	@RequestMapping(value="/home")
	public String home(Model model) {
		model.addAttribute("elist", edao.getAllEmp());
		return "home";
		}

}
