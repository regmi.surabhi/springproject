package com.bway.springproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bway.springproject.dao.UserDao;
import com.bway.springproject.model.User;

@Controller
public class SignUpController {
	@Autowired
	private UserDao udao;

	@RequestMapping(value = "/signup")
	public String getForm() {
		
		return "signup";
	}

	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public String saveUser(@ModelAttribute User user) {
		user.setPassword(DigestUtils.md5DigestAsHex(user.getPassword().getBytes()));
		udao.signup(user);
		
		return "login";
	}
}
