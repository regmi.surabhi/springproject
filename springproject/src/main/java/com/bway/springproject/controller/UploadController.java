package com.bway.springproject.controller;

import java.io.FileOutputStream;
import java.io.IOException;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
//choose pictures to upload
public class UploadController {
	@RequestMapping(value = "/upload")
	public String upload() {
		return "upload";
	}

//to post pictures in gallery
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public String saveImg(@RequestParam("file") MultipartFile file, Model model ,HttpSession session) throws IOException {
		if(StringUtils.isEmpty(session.getAttribute("active"))){
			return "login";
		}
		if (!file.isEmpty()) {
			FileOutputStream out = new FileOutputStream(
					"C:\\Users\\Surabhi\\Documents\\workspace-sts-3.9.11.RELEASE\\springproject\\src\\main\\webapp\\resources\\imgs\\"+file.getOriginalFilename());
			out.write(file.getBytes());
			out.close();
			model.addAttribute("msg", "Upload Success");
			return "upload";
		}

		model.addAttribute("msg", "Upload Failed");
		return "upload";

	}
}