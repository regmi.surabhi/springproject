<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form"
	prefix="spring"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<%@ include file="header.jsp" %>
	<h1>Employee Form</h1>
	<hr>
	<spring:form action="employee" method="post" modelAttribute="emodel">
		<table>
		<tr>
				<td>FirstName</td>

				<td><spring:input path="fname" /></td>

			</tr>
		
			<tr>
				<td>LastName</td>

				<td><spring:input path="lname" /></td>

			</tr>

			<tr>
				<td>Gender</td>

				<td><spring:radiobutton path="gender" value="male" />Male
				<td><spring:radiobutton path="gender" value="female" />Female
				
				</td>
				

			</tr>
			<tr>
				<td>DOB</td>

				<td><spring:input path="dob" type="Date" /></td>

			</tr>
			<tr>
				<td>JoinDate</td>

				<td><spring:input path="joinDate" type="Date" /></td>

			</tr>
			<tr>
				<td>Phone</td>

				<td><spring:input path="phone" /></td>

			</tr>


			<tr>
				<td>Company</td>

				<td><spring:input path="company" /></td>

			</tr>
			<tr>
				<td>Salary</td>

				<td><spring:input path="salary" /></td>

			</tr>
			<tr>
				<td>country</td>

				<td><spring:input path="address.country" /></td>

			</tr>
			
			<tr>
				<td>State</td>

				<td><spring:select path="address.state">
				<spring:option value="">-----select------</spring:option>
				<spring:option value="state-1">state-1</spring:option>
				<spring:option value="state-2">state-2</spring:option>
				<spring:option value="state-3">state-3</spring:option>
				<spring:option value="state-4">state-4</spring:option>
				<spring:option value="state-5">state-5</spring:option>
				<spring:option value="state-6">state-6</spring:option>
				<spring:option value="state-7">state-7</spring:option>
				
				</spring:select>
				</td>
			</tr>
			<tr>
				<td>City</td>

				<td><spring:input path="address.city" /></td>

			</tr>
			 

               <tr>
				<td><input type="submit" value="add"> </td>		

			</tr>

		</table>



	</spring:form>


</body>
</html>