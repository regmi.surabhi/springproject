<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>Home</title>
</head>
<body>
<%@ include file="header.jsp" %>
	<P>Welcome ${user}.</P>
	
	<table id="myTable" class="table table-striped">

		<thead>
			<tr class="success">
				<td>FirstName</td>
				<td>LastName</td>
				<td>Company</td>
				<td>Phone</td>
				<td>City</td>
				<td>Action</td>
				
			</tr>
		

		</thead>
		<tbody>
			<c:forEach var="emp" items="${elist }">
				<tr class="danger">
					<td>${emp.fname}</td>
	        		<td>${emp.lname}</td>
					<td>${emp.company}</td>
					<td>${emp.phone}</td>
					<td>${emp.address.city}</td>
					<td>
					<input  type="submit" onclick="editEmp(${emp.id})" value="Edit" class="  btn btn-success">
					<input  type="submit"onclick="deleteEmp(${emp.id })" value="Delete"class="btn btn-danger">
					</td>

				</tr>
			</c:forEach>

		</tbody>
	</table>
	
	<script type="text/javascript">

function editEmp(id){
	window.location="${pageContext.request.contextPath}/"+id+"/edit";
}

function deleteEmp(id){
	window.location="${pageContext.request.contextPath}/"+id+"/delete";
}
$(document).ready( function () {
    $('#myTable').DataTable();
} );

</script>
</body>
</html>
